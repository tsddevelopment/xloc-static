function init_mobile_header_menus() {


    var menu_trigger = $(".xloc-mobile-menu-trigger");
    var dd_menus = $(".xloc-mobile-menus");

    menu_trigger.click(function () {
        dd_menus.stop().slideToggle();
        menu_trigger.toggleClass("open");
    });


    var triggers = $(".xloc-mobile-menus > .column");

    triggers.click(function () {

        var menu = $(this).find("> ul");

        if ($(this).hasClass("open")) {
            // menu is open and all menus should close
            console.log('menu currently open');
            $(this).removeClass("open");
            menu.stop().slideUp(100);
            return;
        }

        var open_count = $('.xloc-mobile-menus .column.open').length;

        if (open_count == 0) {

            console.log('no other menus open');

            $(this).find("> ul").stop().slideDown();
            $(this).addClass("open");

            return;

        }


        console.log('different menu open');

        triggers.removeClass("open");
        $(this).addClass("open");
        // a different menu is open, needs to be closed and the new menu needs to be open
        triggers.find("> ul").slideUp(100, function () {
            menu.stop().slideDown();
        });

    })

}


$(document).foundation();

init_mobile_header_menus();